// contains business logic/functions/ all operations
//defines the functions to be used in the 'taskRoutes.js'
const Task = require("../models/task.js");

module.exports.getAllTasks = () => {
  return Task.find({})
  .then(result=>{
    return result;
  });
};

module.exports.createTask = (requestBody) => {
  let newTask = new Task ({
    name:requestBody.name
  });
  return newTask.save()
  .then((task, error) => {
    if (error) {
      console.log(error);
      return false;
    }else{
      return task;
    }
  })
}

module.exports.deleteTask =(taskId) => {
  return Task.findByIdAndRemove(taskId)
  .then((result, error) => {
    if(error){
      console.log(error);
      return false;
    }else{
      return result
    }
  })
}

module.exports.updateTask = (taskId, newContent) => {
  return Task.findById(taskId)
  .then ((result,error) => {
    if (error){
      console.log(error);
      return false;
    } else {
      result.name = newContent.name;
      return result.save()
      .then((updatedTask, error) => {
        if (error) {
          console.log(error);
          return false
        } else {
          return updatedTask;
        }
      })
    }
  })
};

// Activity:================================================
// Controller function for retrieving a specific task.
module.exports.getTask = (taskId) => {
  return Task.findById(taskId)
  .then(result=>{
    return result;
  });
};

// Controller function for changing the status of a task to "complete".
module.exports.updateStatus = (taskId) => {
  return Task.findById(taskId)
  .then ((result,error) => {
    if (error){
      console.log(error);
      return false;
    } else {
      result.status = "completed";
      return result.save()
      .then((updatedTask, error) => {
        if (error) {
          console.log(error);
          return false
        } else {
          return updatedTask;
        }
      })
    }
  })
};
