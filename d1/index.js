//app.js/index.js contains all server info

//BASIC IMPORTS
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require('./routes/taskRoutes.js');

//SERVER SETUP
app = express();
const port = 3000;
//MONGODB CONNECT
mongoose.connect("mongodb+srv://mikerodz:Hydepark@wdc028-course-booking.nlohbhy.mongodb.net/b190-to-do?retryWrites=true&w=majority",
{
  useNewUrlParser: true,
  useUnifiedTopology: true
}
);
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open",()=> console.log("Connected to the database."));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/tasks", taskRoutes);
//SERVER CONFIRMATION
app.listen(port, () => console.log(`Server running at port ${port}.`));