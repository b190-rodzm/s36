// models files will import mongoose module

const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: "pending"
  }
});

module.exports = mongoose.model('Task', taskSchema);
